import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Page } from "tns-core-modules/ui/page";
import * as app from "tns-core-modules/application";
import { AuthService } from "~/app/services/auth.service";
import { Store } from "@ngrx/store";
import { IUserState } from "~/app/store/user/state";
import { loginAction } from "~/app/store/user/actions";
import { userSelector } from "~/app/store/user/selectors";

@Component({
    selector: "Login",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {

    private email: string = "dattran@gmail.com";
    constructor(
        private page: Page,
        private auth: AuthService,
        private store: Store<IUserState>
    ) {}

    ngOnInit(): void {
        this.page.actionBarHidden = true;
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    getUser() {
        this.store.select(userSelector).subscribe((data: any) => this.email = data.user.email);
    }

    onLogin() {
        this.store.dispatch(loginAction({email: "admin", password: ""}));
    }

    onLoginWithHttpClient() {
        this.auth.login("admin", "admin").subscribe(
            (data) => {
                console.log(data);
            },
            (err: any) => {
                console.log(err);
            }
        );
    }
}
