import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { httpInterceptorProviders } from "~/app/common/helpers/api/interceptors/http.interceptor";
import { HttpClientModule } from "@angular/common/http";
import { RootStoreModule } from "~/app/store/store.module";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NetworkConnection } from "~/app/classes/NetworkConnection";

import { AuthService } from "~/app/services/auth.service";
import { ErrorService } from "~/app/services/error.service";

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        AppRoutingModule,
        NativeScriptModule,
        NativeScriptUISideDrawerModule,
        NativeScriptHttpClientModule,
        HttpClientModule,
        RootStoreModule
    ],
    declarations: [
        AppComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    providers: [
        NetworkConnection,
        AuthService,
        ErrorService,
        httpInterceptorProviders
    ]
})
export class AppModule { }
