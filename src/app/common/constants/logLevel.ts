/**
 * @author Dat Tran
 * @description
 * @constant LOG_LEVEL
 */
export const LOG_LEVEL = {
    DEFAULT: "DEFAULT",
    WARNING: "WARNING",
    ERROR: "ERROR",
    EXCEPTION: "EXCEPTION"
};
