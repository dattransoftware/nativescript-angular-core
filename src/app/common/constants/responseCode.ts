/**
 * @author Dat Tran
 * @description Response code http
 * @constant RESPONSE_CODE
 */
export const RESPONSE_CODE = {
	UNAUTHORIZED: 401,
	UNPROCESSABLE_ENTITY: 422,
	BAD_REQUEST: 400
};
