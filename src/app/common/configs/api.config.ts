export const APP_CONFIG = {
    API_URL: "http://textsdaily.success-ss.com.vn:8802",
    API_KEY: "{API_URL}",
    END_POINT_KEY: "{END_POINT}",
    API_URL_FORMAT: "{API_URL}/{END_POINT}"
};

export const HTTP_VERBS = {
    GET: "GET",
    POST: "POST",
    PUT: "PUT",
    DELETE: "DELETE"
};

export const API_END_POINT = {
    LOGIN: "test"
};
