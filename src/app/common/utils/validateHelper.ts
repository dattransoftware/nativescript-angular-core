/**
 * Validate if email is valid
 * @returns Boolean
 */
export const validateEmail = (email: string) => {
  return /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/.test(email);
};

/**
 * Validate if password is valid
 * @returns Boolean
 */
export const validatePassword = (password: string) => {
  return Boolean(password && password.length > 5 && password.length < 20);
};

export default {
  validateEmail,
  validatePassword
};
