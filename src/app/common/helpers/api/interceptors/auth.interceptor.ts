import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler, HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { map, catchError } from "rxjs/operators";
import Storage from "~/app/common/helpers/storage/local.storage";
import { RESPONSE_CODE } from "~/app/common/constants/responseCode";

export const AUTH_TOKEN_KEY = "jwt_token";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  /**
   * Set header request
   */
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const currentUser = Storage.get("user");
    const token = Storage.get("token");

    if (currentUser && currentUser.accessToken) {
      request = request.clone({
        headers: request.headers.set(`Authorization`, token)
      });
    }

    if (!request.headers.has("Content-Type")) {
      request = request.clone({
        headers: request.headers.set("Content-Type", "application/json")
      });
    }

    request = request.clone({
      headers: request.headers.set("Accept", "application/json")
    });

    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          console.log("event--->>>", event);
        }

        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        if (error.status === RESPONSE_CODE.UNAUTHORIZED) {
          Storage.set("token", "");
          Storage.set("user", "");
          console.log("Your session has timed out. Please log in again.");

          return;
        }

        return throwError(error);
      })
    );
  }
}
