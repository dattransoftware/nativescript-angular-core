import * as localStorage from "tns-core-modules/application-settings";

const get = (key = "") => {
	const value = localStorage.getString(key);
	if (value) {
		return JSON.parse(value);
	}

	return value;
};

const set = (key = "", value = "") => {
	return localStorage.setString(key, JSON.stringify(value));
};

const remove = (key = "") => {
	return localStorage.remove(key);
};

export default {
	get,
	set,
	remove
};
