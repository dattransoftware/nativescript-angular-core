export interface IUserState {
    token: string;
    user: object;
}

export const userState: IUserState = {
    token: "",
    user: {}
};
