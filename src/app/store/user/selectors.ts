import { createSelector, createFeatureSelector } from "@ngrx/store";
import { IUserState } from "./state";
import { FEATURE_NAME } from "./reducer";

export const getUser = createFeatureSelector<IUserState>(FEATURE_NAME);
export const userSelector = createSelector(getUser, (data) => data);
