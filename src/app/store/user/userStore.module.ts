import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";
import { UserEffects } from "./effects";
import { reducer, FEATURE_NAME } from "./reducer";

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(FEATURE_NAME, reducer),
    EffectsModule.forFeature([UserEffects])
  ],
  providers: [UserEffects]
})
export class UserStoreModule { }