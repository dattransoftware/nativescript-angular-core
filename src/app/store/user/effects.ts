import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { of } from "rxjs";
import { map, catchError, exhaustMap  } from "rxjs/operators";
import { AuthService } from "~/app/services/auth.service";
import { loginAction, loginFailure, loginSuccess } from "./actions";

@Injectable()
export class UserEffects {

    login$ = createEffect(() =>
        this.actions$.pipe(
        ofType(loginAction),
        exhaustMap((action) =>
            this.authService.login(action.email, action.password).pipe(
            map((data) => loginSuccess({payload: { data }})),
            catchError((error) => of(loginFailure({payload: error })))
            )
        )
        )
    );

    constructor(
        private actions$: Actions,
        private authService: AuthService
    ) {}

}
