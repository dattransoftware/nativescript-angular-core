const _ACTION = {
    LOGIN: "[Login Page] Login",
    LOGIN_SUCCESS: "[Auth API] Login Success",
    LOGIN_FAILURE: "[Auth API] Login Failure"
};

export default {
    _ACTION
};
