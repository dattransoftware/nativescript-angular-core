import { Action, createReducer, on, State } from "@ngrx/store";
import * as actions from "./actions";
import { userState, IUserState } from "./state";

const loginReducer = createReducer(
    userState,
    on(actions.loginSuccess, (state, action) => {
        return {
            ...state,
            token: action.payload.data.token,
            user: action.payload.data.user
        };
    }),
    on(actions.loginFailure, (state, action) => ({
        ...state,
        error: action.payload.error
    }))
);

export function reducer(state: IUserState | undefined, action: Action) {
    return loginReducer(state, action);
}

export const FEATURE_NAME = "login";
