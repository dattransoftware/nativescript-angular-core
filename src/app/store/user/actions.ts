import { createAction, props } from "@ngrx/store";
import CONST from "./actionTypes";

export const loginAction = createAction(
    CONST._ACTION.LOGIN,
    props<{ email: string; password: string }>()
);

export const loginSuccess = createAction(
    CONST._ACTION.LOGIN_SUCCESS,
    props<{ payload: { data } }>()
);

export const loginFailure = createAction(
    CONST._ACTION.LOGIN_FAILURE,
    props<{ payload: { error } }>()
);
