import { Injectable } from "@angular/core";
import { HttpService } from "~/app/services/http.service";
import { API_END_POINT } from "~/app/common/configs/api.config";

@Injectable()
export class AuthService {

    constructor(private http: HttpService) {}

    /**
     * @author Dat Tran
     * @description Get token login
     * @param email string
     * @param password string
     * @returns Observable
     */
    login(email: string, password: string) {
        return this.http.get(API_END_POINT.LOGIN);
    }
}
