import { Injectable } from "@angular/core";
import { log } from "~/app/common/utils/logHelper";
import { LOG_LEVEL } from "~/app/common/constants/logLevel";

@Injectable()
export class ErrorService {
  constructor() {}

  handleError = (error: Error) => {
    log(error, LOG_LEVEL.ERROR);
  }

  handleException = (exception: Error) => {
    log(exception, LOG_LEVEL.EXCEPTION);
  }
}
